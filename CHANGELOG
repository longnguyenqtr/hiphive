0.5
---
* IO functions for Optimizer
* updated rotational sum rules example
* metadata added to ForceConstantsPotential
* various small fixes to documentation

0.4.1
-----
* functionality for reconstructing force constants obtained from e.g., phonopy
* native rattle module
* ForceConstants object now supports read and write
* improved documentation
* improved Cutoffs object
* improved support for recursive feature elimination (RFE)
* code clean up
* overnight builds

0.4
---
* added cluster filter functionality
* fixed and improved IO functions for force constants parsing from phonopy, phono3py, and ShengBTE
* improved interface for optimization with recursive feature elimination
* ForceConstants API
* extended additional topics (including interfacing DFT codes with hiphive)
* bug fixes (related to numerical tolerance and others)

0.3
---
* large speed up of sensing matrix calculation
* self-consistent phonons
* spectral energy density
* new advanced tutorial topics
* smaller bug fixes and improvements

0.2
---
* much improved performance
* improved numerical stability
* improved memory management
* structure container storage uses data compression
* additional unit tests; increased code coverage
* improved code quality thanks to extensive refactoring
* bug fixes
