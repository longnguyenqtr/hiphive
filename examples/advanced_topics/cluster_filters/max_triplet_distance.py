"""
Use of a ClusterFilter to reduce ClusterSpace
"""
from ase.build import bulk
from hiphive import ClusterSpace
from hiphive.cutoffs import CutoffMaximumBody, MaxTripletDistance

prim = bulk('Ti')
cutoffs = CutoffMaximumBody([6, 6, 6], max_nbody=3)

cluster_filter = MaxTripletDistance(4)
cs_filter = ClusterSpace(prim, cutoffs=cutoffs, cluster_filter=cluster_filter)

cs_no_filter = ClusterSpace(prim, cutoffs=cutoffs)


print('Clusterspace with filter:')
print('Number of orbits: {}'.format(len(cs_filter.orbits)))
print('Number of parameters: {}'.format(cs_filter.n_dofs))

print('Clusterspace without filter:')
print('Number of orbits: {}'.format(len(cs_no_filter.orbits)))
print('Number of parameters: {}'.format(cs_no_filter.n_dofs))
