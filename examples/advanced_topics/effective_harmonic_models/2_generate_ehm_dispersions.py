import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.cm as cmx

from ase import Atoms
from ase.build import bulk
from ase.io import read

from phonopy import Phonopy
from phonopy.structure.atoms import PhonopyAtoms

from hiphive import ClusterSpace, StructureContainer, ForceConstantPotential
from hiphive.fitting import Optimizer


def get_band(q_start, q_stop, N):
    """ Return path between q_start and q_stop """
    return np.array([q_start + (q_stop-q_start)*i/(N-1) for i in range(N)])


# parameters
THz2meV = 4.13567
cutoffs = [6.5, 3.0]
temperatures = [200, 800, 1400]
dim = 5
prim = bulk('Ni')

# set up phonon band paths
Nq = 51
G2X = get_band(np.array([0, 0, 0]), np.array([0.5, 0.5, 0]), Nq)
X2K2G = get_band(np.array([0.5, 0.5, 1.0]), np.array([0, 0, 0]), Nq)
G2L = get_band(np.array([0, 0, 0]), np.array([0.5, 0.5, 0.5]), Nq)
bands = [G2X, X2K2G, G2L]

# set up ClusterSpace and StructureContainer
cs = ClusterSpace(prim, cutoffs)
print(cs)
cs.print_orbits()

sc = StructureContainer(cs)
user_tag = 'md-T{}'
for temperature in temperatures:
    structures = read('md_runs/snapshots_T{:}.xyz@:'.format(temperature))
    for structure in structures:
        sc.add_structure(structure, user_tag=user_tag.format(temperature))
print(sc)

# construct effective harmonic dispersions
band_structures = []
for temperature in temperatures:

    # Construct ForceConstantPotential
    structure_indices = [i for i, fs in enumerate(sc)
                         if fs.user_tag == user_tag.format(temperature)]
    opt = Optimizer(sc.get_fit_data(structure_indices), train_size=1.0)
    opt.train()
    print(opt)
    fcp = ForceConstantPotential(cs, opt.parameters)

    # setup phonopy and get FCs
    atoms_phonopy = PhonopyAtoms(symbols=prim.get_chemical_symbols(),
                                 scaled_positions=prim.get_scaled_positions(),
                                 cell=prim.cell)
    phonopy = Phonopy(atoms_phonopy, supercell_matrix=dim*np.eye(3),
                      primitive_matrix=None)
    supercell = phonopy.get_supercell()
    supercell = Atoms(cell=supercell.cell, numbers=supercell.numbers, pbc=True,
                      scaled_positions=supercell.get_scaled_positions())

    fcs = fcp.get_force_constants(supercell)
    phonopy.set_force_constants(fcs.get_fc_array(order=2))

    # get phonon dispersion
    phonopy.set_band_structure(bands)
    qvecs, qnorms, freqs, _ = phonopy.get_band_structure()
    band_structures.append([temperature, qnorms, freqs])


# plot dispersions
fig = plt.figure()
lw = 2.0
fs = 14.0

kpts = [0.0, qnorms[0][-1], qnorms[1][-1], qnorms[2][-1]]
kpts_labels = ['$\\Gamma$', 'X', '$\\Gamma$', 'L']

cm = plt.get_cmap('viridis')
cNorm = mcolors.Normalize(vmin=0, vmax=1)
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cm)
colors = [scalarMap.to_rgba(float(k) / (len(band_structures) - 1))
          for k in range(len(band_structures))]

plt.axvline(x=kpts[1], color='k', linewidth=0.9)
plt.axvline(x=kpts[2], color='k', linewidth=0.9)

for color, (T, qnorms, freqs) in zip(colors, band_structures):

    for q, freq, in zip(qnorms, freqs):
        plt.plot(q, freq * THz2meV, color=color, lw=lw)
    plt.plot(np.nan, np.nan, color=color, lw=lw, label='T = {}'.format(T))

plt.xlabel('Wave vector $\\vec{q}$', fontsize=fs)
plt.ylabel('$\\omega$ (meV)', fontsize=fs)
plt.xticks(kpts, kpts_labels, fontsize=fs)
plt.xlim([0.0, qnorms[-1][-1]])
plt.ylim([0.0, 50.0])

plt.legend()
plt.tight_layout()
plt.savefig('phonon_dispersions_ehm.svg')
