.. module:: hiphive

Structures
==========

.. index::
   single: Class reference; StructureContainer

StructureContainer
------------------

.. autoclass:: StructureContainer
   :members:


.. index::
   single: Class reference; FitStructure

FitStructure
------------

.. autoclass:: hiphive.structure_container.FitStructure
   :members:


.. index::
   single: Structure generation

Structure generation
--------------------

.. automodule:: hiphive.structure_generation
   :members:
   :undoc-members:
   :noindex:



Other functions
---------------

.. autofunction:: hiphive.structure_container.are_configurations_equal
   :noindex:
