.. _advanced_topics:
.. index::
   single: Advanced topics

Advanced topics
***************

The following tutorial sections illustrate applications of
:program:`hiPhive`.

Some of the following examples employ `phonopy
<https://atztogo.github.io/phonopy/>`_ and `phono3py
<https://atztogo.github.io/phono3py/>`_ for analyzing force constants from
:program:`hiPhive`. Please consult the respective websites for installation
instructions.

The scripts and database that are required for the advanced topics can
be `downloaded as a single zip archive
<https://hiphive.materialsmodeling.org/advanced_topics.zip>`_. These
scripts will be compatible with the latest stable release. If you want
to download the scripst from the development version `download this
archive
<https://hiphive.materialsmodeling.org/dev/advanced_topics.zip>`_
instead.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   structure_generation
   reference_calculations
   cutoffs_and_cluster_filters
   cluster_analysis
   force_constants_io
   learning_curve
   feature_selection
   interface_with_sklearn
   compare_fcs
   effective_harmonic_models
   self_consistent_phonons
   anharmonic_energy_surface
   rotational_sum_rules
   fcs_sensing

