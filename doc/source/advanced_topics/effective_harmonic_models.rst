.. _advanced_topics_effective_harmonic_models:
.. highlight:: python
.. index::
   single: Effective harmonic models

Effective harmonic models
=========================

Using :program:`hiPhive` it is straightforward to generate effective harmonic
models (EHMs) from molecular dynamics (MD) trajectories. These models enable
one to describe one for example the temperature dependence of the phonon
dispersion as well as derived quantities such as the harmonic free energy.

This example comprises two scripts: The first one carries out a series of MD
simulations using the EMT calculator from ASE, while the second scripts
generates EHMs for each temperature and analyzes the resulting phonon
dispersions.

We use cutoffs of 6.5 and 3.0 Å for second and third order, respectively. Note
that the third order term is only included to reduce the noise in the fit.
These cutoffs yields the following orbits::

  ===================================== List of Orbits =====================================
  index | order |      elements      |  radius  |     prototype      | clusters | parameters
  ------------------------------------------------------------------------------------------
    0   |   2   |       Ni Ni        |  0.0000  |       (0, 0)       |    1     |     1
    1   |   2   |       Ni Ni        |  1.2445  |       (0, 1)       |    6     |     3
    2   |   2   |       Ni Ni        |  2.4890  |       (0, 2)       |    6     |     3
    3   |   2   |       Ni Ni        |  2.1556  |       (0, 3)       |    12    |     4
    4   |   2   |       Ni Ni        |  3.0484  |      (0, 10)       |    4     |     2
    5   |   2   |       Ni Ni        |  2.7828  |      (0, 11)       |    12    |     4
    6   |   2   |       Ni Ni        |  1.7600  |      (0, 16)       |    3     |     2
    7   |   3   |      Ni Ni Ni      |  1.1062  |     (0, 0, 1)      |    12    |     5
    8   |   3   |      Ni Ni Ni      |  1.4370  |     (0, 1, 5)      |    8     |     7
  ==========================================================================================

With increasing temperature the ability of an EHM to map the forces present in
the structure decreases as evident from the root-mean-square errors over the
training set:

    =============== ==================
    Temperature (K) Train RMSE (meV/A)
    =============== ==================
    200              11.3
    800              94.9
    1400            221.1
    =============== ==================

The phonon dispersions obtained in this fashion are shown in the following
figure.

.. figure:: _static/phonon_dispersions_ehm.svg

  Phonon dispersions for FCC Ni from a series of effective harmonic models
  generated from MD trajectories recorded at different temperatures.

Source code
-----------

.. |br| raw:: html

   <br />

.. container:: toggle

    .. container:: header

       MD structures are generated in |br|
       ``examples/advanced_topics/effective_harmonic_models/1_run_md_simulations.py``

    .. literalinclude:: ../../../examples/advanced_topics/effective_harmonic_models/1_run_md_simulations.py

.. container:: toggle

    .. container:: header

       Effective harmonic models are constructed and their phonon dispersions plotted in |br|
       ``examples/advanced_topics/effective_harmonic_models/2_generate_ehm_dispersions.py``

    .. literalinclude:: ../../../examples/advanced_topics/effective_harmonic_models/2_generate_ehm_dispersions.py
